//
//  MeasurementsVC.swift
//  Measurements
//
//  Created by Shafqat Muneer on 12/22/18.
//  Copyright © 2018 Shafqat Muneer. All rights reserved.
//

import UIKit

public enum Type {
    case height
    case waist
    case hips
    case braSize
}

class MeasurementsVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var skipButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        initialization()
    }
    
    func initialization() {
        //Hide shadow of navigation bar
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        //Add back button
        let button: UIButton = UIButton(type: .custom)
        button.setImage(UIImage(named: "BackButton"), for: .normal)
        button.addTarget(self, action: #selector(self.backButtonTapped), for: .touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 45, height: 10)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
        nextButton.titleEdgeInsets = UIEdgeInsets(top: -16, left: 0, bottom: 0, right: 0)
    }
    
    @objc func backButtonTapped(sender: UIButton) {
        showFunctionalityNotRequiredAlertMessage()
    }
    
    @IBAction func didTapNextButton(_ sender: Any) {
        showFunctionalityNotRequiredAlertMessage()
    }
    @IBAction func didTapSkipButton(_ sender: Any) {
        showFunctionalityNotRequiredAlertMessage()
    }
    
    private func showFunctionalityNotRequiredAlertMessage() {
        let alert = UIAlertController(title: nil, message: K.FUNCTIONALITY_NOT_REQUIRED, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            var cell : UITableViewCell!
            cell = tableView.dequeueReusableCell(withIdentifier: "LabelCell")
            if cell == nil {
                cell = UITableViewCell(style: .default, reuseIdentifier: "LabelCell")
            }
            return cell
        } else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: DoubleColumnCell.cellIdentifier, for: indexPath) as! DoubleColumnCell
            cell.configureCellData(cellType: .height)
            return cell
        } else if indexPath.row == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: SingleColumnCell.cellIdentifier, for: indexPath) as! SingleColumnCell
            cell.configureCellData(cellType: .waist)
            return cell
        } else if indexPath.row == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: SingleColumnCell.cellIdentifier, for: indexPath) as! SingleColumnCell
            cell.configureCellData(cellType: .hips)
            return cell
        } else if indexPath.row == 4 {
            let cell = tableView.dequeueReusableCell(withIdentifier: DoubleColumnCell.cellIdentifier, for: indexPath) as! DoubleColumnCell
            cell.configureCellData(cellType: .braSize)
            return cell
        }
        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 105.0
        } else {
            return 82.0
        }
    }
}
