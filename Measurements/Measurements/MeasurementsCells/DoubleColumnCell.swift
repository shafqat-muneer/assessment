//
//  DoubleColumnCell.swift
//  Measurements
//
//  Created by Shafqat Muneer on 12/23/18.
//  Copyright © 2018 Shafqat Muneer. All rights reserved.
//

import UIKit

class DoubleColumnCell: UITableViewCell {

    static let cellIdentifier = "DoubleColumnCell"
    @IBOutlet weak var firstColumnButton: UIButton!
    @IBOutlet weak var secondColumnButton: UIButton!
    @IBOutlet weak var dataTypeLabel: UILabel!
    @IBOutlet weak var firstColumnMeasurementLabel: UILabel!
    @IBOutlet weak var secondColumnMeasurementLabel: UILabel!
    @IBOutlet weak var notSelectedValueHighlighter: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        firstColumnButton.contentVerticalAlignment = .bottom
        secondColumnButton.contentVerticalAlignment = .bottom
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    func configureCellData(cellType:Type) {
        switch cellType {
        case .height:
            dataTypeLabel.text = K.HEIGHT
            firstColumnMeasurementLabel.text = K.FEET
            secondColumnMeasurementLabel.text = K.INCH
        case .braSize:
            dataTypeLabel.text = K.BRA_SIZE
            firstColumnMeasurementLabel.text = K.BAND
            secondColumnMeasurementLabel.text = K.CUP
        default: break
        }
    }
    
    @IBAction func didTapFirstColumnButton(_ sender: Any) {
    }
    
    @IBAction func didTapSecondColumnButton(_ sender: Any) {
    }
}
