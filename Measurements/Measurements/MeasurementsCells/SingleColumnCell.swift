//
//  SingleColumnCell.swift
//  Measurements
//
//  Created by Shafqat Muneer on 12/23/18.
//  Copyright © 2018 Shafqat Muneer. All rights reserved.
//

import UIKit

class SingleColumnCell: UITableViewCell {

    static let cellIdentifier = "SingleColumnCell"
    @IBOutlet weak var selectionButton: UIButton!
    @IBOutlet weak var dataTypeLabel: UILabel!
    @IBOutlet weak var notSelectedValueHighlighter: UIView!
    @IBOutlet weak var hintLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCellData(cellType:Type) {
        switch cellType {
        case .waist:
            dataTypeLabel.text = K.WAIST
            hintLabel.text = K.HINT_LABEL_FOR_WAIST
        case .hips:
            dataTypeLabel.text = K.HIPS
            hintLabel.text = K.HINT_LABEL_FOR_HIPS
        default: break
        }
    }
    
    @IBAction func didTapSelectionButton(_ sender: Any) {
        
    }

}
