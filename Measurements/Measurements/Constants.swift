//
//  Constants.swift
//  Measurements
//
//  Created by Shafqat Muneer on 12/23/18.
//  Copyright © 2018 Shafqat Muneer. All rights reserved.
//

import Foundation

struct K {
    static let HEIGHT = "Height"
    static let BRA_SIZE = "Bra size"
    static let BAND = "band"
    static let CUP = "cup"
    static let FEET = "ft"
    static let INCH = "in"
    static let WAIST = "Waist"
    static let HINT_LABEL_FOR_WAIST = "Not sure? Select a top size"
    static let HIPS = "Hips"
    static let HINT_LABEL_FOR_HIPS = "Not sure? Select a bottom size"
    static let FUNCTIONALITY_NOT_REQUIRED = "Functionality not required at this stage."
    
    static let HEIGHTS_IN_FEETS = ["4", "5", "6"]
    static let HEIGHTS_IN_INCHES = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11"]
    static let BANDS = ["28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48"]
    static let CUPS = ["AA", "A", "B", "C", "D", "DD", "DDD", "DDDD", "H", "I", "J", "K"]
    static let WAISTS = ["20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50"]
    static let HIPSS = ["30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "60"]
}
